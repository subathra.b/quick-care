
function openTab(evt, tabName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName('tabcontent');
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = 'none';
    }

    tablinks = document.getElementsByClassName('tablinks');
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace('active', '');

    }
    if (tabName == 'question') {
        document.getElementById('back').style.display = 'none';
    }
    else {
        document.getElementById('back').style.display = 'block';
    }

    document.getElementById(tabName).style.display = 'block';
    evt.currentTarget.className += ' active';
}
function addInput() {
    var ul = document.getElementById('medication-name');
    var medication_inp = document.getElementById('input-value');
    var li = document.createElement('li');
    var img = document.createElement("img");
    img.setAttribute('src', '../images/close.png')
    img.setAttribute("height", "30");
    img.setAttribute("width", "30");  
    
    document.getElementById('medication-name').style.listStyleType = 'none';
    li.setAttribute('id',  medication_inp.value);
    li.appendChild(document.createTextNode(medication_inp.value));
    ul.appendChild(li);
    li.appendChild(img);
    img.addEventListener('onclick', removeItem)
}
function removeItem()
{
    alert()
    var ul = document.getElementById("medication-name");
    var medication_inp = document.getElementById("input-value");
    var item = document.getElementById(medication_inp.value);
    console.log(item)
    ul.removeChild(item);
}

